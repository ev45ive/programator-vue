
export interface Playlist {
    id: number 
    name: string
    favorite: boolean
    color: string
    tracks?: Track[]
}

interface Track {
    id: number
    name: string
}

// ==============

// let x = 1
//     x = 'test' // Type '"test"' is not assignable to type 'number'

let p: Playlist = {
    id: 123,
    name: '123',
    color: '',
    favorite: true
}
// if ('string' == p.id) {
//     p.id.bold()
// } else {
//     p.id
// }