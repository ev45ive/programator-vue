import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import resource from 'vue-resource'

Vue.use(resource)

Vue.config.productionTip = false;

// import Playlists from './views/Playlists.vue'
// Vue.component('Playlists', Playlists)

// Vue.component('Button', Button)



new Vue({
  router,
  store,
  render: h => h(App)
})
  .$mount("#app");
