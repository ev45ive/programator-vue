# Git 
cd ../../Desktop
git clone https://bitbucket.org/ev45ive/programator-vue


# Wersje
https://nodejs.org/en/
node -v 
npm -v 

https://code.visualstudio.com/
code -v 

https://git-scm.com/download/win
git --version

# Http server
<!-- %PATH% -->
npm install --global http-server
<!-- lub -->
npm i -g http-server
<!-- uruchom w TYM katalogu -->
http-server.cmd
http://localhost:8080/

# Nowy projekt
cd vue-simple
npm init -y
vue-simple\package.json

# Instalowanie lokalnie w projekcie + zapamietaj
npm i --save http-server 
<!-- lub -->
npm i --save-dev http-server  
<!-- package.json -->
  "scripts": {
    "start": "http-server",
<!-- terminal -->
npm run start <!-- lub --> npm start

# Git
git init
echo vue-simple/node_modules > .gitignore
git add .
git status
git commit -m "Commit mesage"
git log

# Biblioteki
npm i --save bootstrap vue jquery 


# Vetur
> Extensions > Vetur -> Install
https://marketplace.visualstudio.com/items?itemName=octref.vetur

# Vue CLI
npm install -g @vue/cli

vue --help
pwd

vue.cmd create vue-cli-projekt 

  > Manually select features
  > All but :
    X Unit testing 
    X E2E Testing
  > Use class-style component syntax? =>  (Y)
  > Use Babel alongside TypeScript => (Y)
  > Use history mode for router?  => (N)
  > Pick a CSS pre-processor  
    >  Sass/SCSS (with node-sass)
  <!-- > Pick a linter / formatter config 
    > ESLint + Prettier -->
  > Pick additional lint features  
    > Lint on save
  Where do you prefer placing config for Babel, ESLint, etc.? (Use arrow keys)
    > In dedicated config files
  Save this as a preset for future projects? (y/N) => (Y)
    > MojUlbuiony

# Run
npm remove @vue/cli-plugin-eslint

npm i --save bootstrap

npm run serve

# Json-Server
npm i -g json-server 

