

const app = new Vue({
    el: '#moja_appka',
    data: {
        komunikat: {
            message: 'Lubie placki',
        }
    },
    methods: {
        funkcja(event) { this.komunikat.message = event.target.value },
        wyczysc(){ this.komunikat.message = '' }
    },
    template: `<div>
        <div class="form-group">
            <input type="text" class="form-control" 
                v-bind:value="komunikat.message"
                v-on:keyup=" funkcja ">
        </div>
        
        <div v-on:click="wyczysc">
            {{ komunikat.message }}
        </div>
    </div>`
})